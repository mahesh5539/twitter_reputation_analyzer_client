var express = require('express');
var router = express.Router();
var mq_client = require('../rpc/client');



/* GET home page. */
router.get('/', function(req, res) {
  res.render('index');
});
router.get('/getProfileInfo/:screen_name', function(req, res) {
	var twitter_handle = req.params.screen_name;
	var msg_payload = { "twitter_handle": twitter_handle, "apiId": "API1" };
	
	mq_client.make_request('profile_queue',msg_payload, function(err,results){		
		console.log(results);
		if(err){
			res.status(500).json(err.message);
		}
		else 
		{
			res.status(results.code).json(results);
		}  
	});
});

router.get('/getAllTweets/:screen_name', function(req, res) {
	var twitter_handle = req.params.screen_name;
	var msg_payload = { "twitter_handle": twitter_handle, "apiId": "API2" };
	
	mq_client.make_request('profile_queue',msg_payload, function(err,results){		
		console.log(results.tweetArray);
		if(err){
			res.status(500).json(err.message);
		}
		else 
		{
			res.status(results.code).json(results);
		}  
	});
});
module.exports = router;
