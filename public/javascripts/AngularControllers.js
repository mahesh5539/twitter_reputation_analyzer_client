 
      var TwitterApp = angular.module('TwitterApp',[]);
      TwitterApp.controller('TwitterController', function($scope,$http,$rootScope){
    	  
    	  TwitterApp.config(['$httpProvider', function($httpProvider) {
    	        $httpProvider.defaults.useXDomain = true;
    	        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    	    }
    	]);
    	  
    	  $scope.selectedItem = "Select option";
    	  $scope.items = ['Select option','Retweet count','Date','With photo', 'Without photo'];

        $scope.getProfile = function(){
          var screen_name = $scope.screen_name;
            $http({
                   method: 'GET',
                   url: '/getProfileInfo/'+screen_name
                   }).success(function(response){
                      var imageURL = response.value[0].user.profile_image_url;
                      var index = imageURL.indexOf("_normal");
                      var endURL = imageURL.slice(index+7);
                      imageURL = imageURL.slice(0,index);
                      imageURL = imageURL+endURL;
                      $scope.imageURL = imageURL;
                      $scope.name = response.value[0].user.name;
                      $scope.screenName = response.value[0].user.screen_name;
                      $scope.description = response.value[0].user.description;
                      $scope.location = response.value[0].user.location;
                      var createdDate = response.value[0].user.created_at;
                      var month = createdDate.slice(4,7);
                      var date = createdDate.slice(8,10);
                      var year = createdDate.slice(26,30);
                      $scope.date = month+' / '+date+' / '+year;
                      $scope.no_of_tweets = response.value[0].user.statuses_count;
                      $scope.following = response.value[0].user.friends_count;
                      $scope.follower = response.value[0].user.followers_count;
                      $scope.getAllTweets($scope.follower);
                      
                    }).error(function(error){
                       alert("Error : user not found!!");
                    });
          };

           $scope.getAllTweets = function(follower){
            var screen_name = $scope.screen_name;
             $http({
                   method: 'GET',
                   url: '/getAllTweets/'+screen_name
                   }).success(function(response){
                	 $rootScope.tweetJson = response.tweetArray;
                     $scope.tweettxts = response.tweetArray;   
                     $scope.getScore(follower);
                     
                    }).error(function(error){
                       alert("Error : Failed to get data!!");
                    });
          };
          
          $scope.orderByretweet = function(JsonArray, key){
        	  function sortByKey(a, b) {
        	        var x = a[key];
        	        var y = b[key];
        	        return ((x > y) ? -1 : ((x < y) ? 1 : 0));
        	    }
        	  JsonArray.sort(sortByKey);
          };
          
          function orderByDate(a,b){
        	  return new Date(a.date).getTime() - new Date(b.date).getTime();
          }
          
          $scope.showByOrder = function(){
       
        	  if($scope.selectedItem==='Retweet count' && $rootScope.tweetJson.length>1){
        		  var JsonToSortRetwt = $rootScope.tweetJson;
        		  $scope.orderByretweet(JsonToSortRetwt,'retweet_count');
        		  $scope.tweettxts = JsonToSortRetwt;
        	  }
        	  
        	  if($scope.selectedItem==='Date' && $rootScope.tweetJson.length>1){
        		  var JsonToSortDate = $rootScope.tweetJson;
        		  JsonToSortDate.sort(orderByDate);
        		  $scope.tweettxts = JsonToSortDate;
        	  }
        	  
        	  if($scope.selectedItem==='With photo' && $rootScope.tweetJson.length>1){
        		  var JsonToSortWithPhoto = $rootScope.tweetJson;
        		  var JsonWithPhotoResult = [];
        		  var count =0;
        		  for(var i=0; i<JsonToSortWithPhoto.length;i++){
        			  if(JsonToSortWithPhoto[i].media=="photo"){
        				  JsonWithPhotoResult[count]=(JsonToSortWithPhoto[i]);
        				  count++;
        			  }
        		  }
        		  $scope.tweettxts = JsonWithPhotoResult;
        	  }
        	  
        	  if($scope.selectedItem==='Without photo' && $rootScope.tweetJson.length>1){
        		  var JsonToSortWithoutPhoto = $rootScope.tweetJson;
        		  var JsonWithoutPhotoResult = [];
        		  var count =0;
        		  for(var i=0; i<JsonToSortWithoutPhoto.length;i++){
        			  if(JsonToSortWithoutPhoto[i].media=="without_photo"){
        				  JsonWithoutPhotoResult[count]=(JsonToSortWithoutPhoto[i]);
        				  count++;
        			  }
        		  }
        		  $scope.tweettxts = JsonWithoutPhotoResult;
        	  }
          };
        $scope.getScore = function(follower){
        	
        	var TweetJson = $rootScope.tweetJson;
        	var ScoreTweetJson =[];
        	
        	for(var i=0;i<TweetJson.length;i++){
        		ScoreTweetJson[i]=TweetJson[i].tweet;
        	}
        	var tweetString = ScoreTweetJson.toString();
        	console.log(tweetString);
        	$http({
                method: 'POST',
                url: 'http://localhost:8081/ScoreCalculator/getReputation/'+follower,
                headers: { 'Accept':'application/json',
                	'Content-Type' : 'application/json;charset=UTF-8'},
                data:{"tweet":tweetString}
                }).success(function(response){
                	$scope.finalScore = response.finalScore; 
                	$scope.positiveWordSocre = response.positiveScore; 
                	$scope.negativeWordScore = response.negativeScore;
                	  
                 }).error(function(error){
                    alert("Error : Failed to get data!!");
                 });
        } 
      
          
          
      });
    